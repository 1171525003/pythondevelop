#coding=utf-8
#函数：
#过程：过程就是没有返回值的函数
def test1(x):
    x+=1
a1=test1(3)
print(a1)

def test2(x):
    x+=1
    return x
a2=test2(3)
print(a2)

#没有返回值就是None

def test3(x):
    x+=1
    return 1,2,[11,22],(33,44),None
a3=test3(3)
print(a3)

#总结：
#返回值=0，返回None
#返回值=1，返回object
#返回值>1:返回tuple