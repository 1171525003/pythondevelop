#coding=utf-8
#形参变量，只有在被调用时，才会被分配内存单元，在调用结束时，即可释放所分配的内存单元，所以形参只在函数内部有效
#在函数调用结束返回主调用函数后，则不能在使用该形参变量
#实参可以是常量，变量，表达式，函数等，无论实参是何种类型的量，在进行函数调用的时候，他们必须有确定的值，以便
#将这些值传递给形参

def calc(x,y):  #形参，调用结束之后接关闭
    res=x**y
    return res

c=calc(3,4) #实参，将值传递给形参-常量
print(c)
a=2
b=2
print(calc(a,b)) #变量的形式
#函数一遇到return的话就结束，这个要记住，不执行下面的代码
#局部变量和全局变量


def test1(x,y,z):
    print(x)
    print(y)
    print(z)
test1(1,2,3) #位置参数，一一对应，不能少任何一个，否则会报错
test1(x=1,y=3,z=4) #关键字参数，直接将对应关系写入实参,也不能缺少参数
#test1(1,y=2,3)  会报错，因为位置参数必须要在关键字左边
#test1(1,3,y=2)  还是会报错，因为位置参数将3传给了y，此时在给y会出现重复
test1(1,3,z=2)  #正确


def test2(x,type="wang"):
    print(x,type)

test2(2,type="chao")  #默认参数 当不传递值的时候为形参默认的值，重新传值之后为新的实参值


#参数组 **args=>字典 ,*args=>列表 [args会当成元组的方式去处理]
def test3(a,*args):
    print(a)
    print(args)
    print(args[0])
test3(1,2,3,4,5,6)
test3(1,["22","33","44"])
test3(1,*[11,22,33,44])

def test4(x,**kwargs):
    print(x)
    print(kwargs)

test4(1, a=1, b=2)
test4(1,**{"name":1})


a="123    123  "
print(a)
print(a.strip())


# #1、位置参数：按照从左到右的顺序定义的参数
#         位置形参：必选参数
#         位置实参：按照位置给形参传值
#
# #2、关键字参数：按照key=value的形式定义的实参
#         无需按照位置为形参传值
#         注意的问题：
#                 1. 关键字实参必须在位置实参右面
#                 2. 对同一个形参不能重复传值
#
# #3、默认参数：形参在定义时就已经为其赋值
#         可以传值也可以不传值，经常需要变得参数定义成位置形参，变化较小的参数定义成默认参数（形参）
#         注意的问题：
#                 1. 只在定义时赋值一次
#                 2. 默认参数的定义应该在位置形参右面
#                 3. 默认参数通常应该定义成不可变类型
#
#
# #4、可变长参数：
#         可变长指的是实参值的个数不固定
#         而实参有按位置和按关键字两种形式定义，针对这两种形式的可变长，形参对应有两种解决方案来完整地存放它们，分别是*args，**kwargs
#
#         ===========*args===========
def foo(x,y,*args):
    print(x,y)
    print(args)
foo(1,2,3,4,5)

def foo(x,y,*args):
    print(x,y)
    print(args)
foo(1,2,*[3,4,5])
#
#
#         def foo(x,y,z):
#             print(x,y,z)
#         foo(*[1,2,3])
#
#         ===========**kwargs===========
#         def foo(x,y,**kwargs):
#             print(x,y)
#             print(kwargs)
#         foo(1,y=2,a=1,b=2,c=3)
#
#         def foo(x,y,**kwargs):
#             print(x,y)
#             print(kwargs)
#         foo(1,y=2,**{'a':1,'b':2,'c':3})
#
#
#         def foo(x,y,z):
#             print(x,y,z)
#         foo(**{'z':1,'x':2,'y':3})
#
#         ===========*args+**kwargs===========
#
#         def foo(x,y):
#             print(x,y)
#
#         def wrapper(*args,**kwargs):
#             print('====>')
#             foo(*args,**kwargs)
#
# #5、命名关键字参数：*后定义的参数，必须被传值（有默认值的除外），且必须按照关键字实参的形式传递
# 可以保证，传入的参数中一定包含某些关键字
#         def foo(x,y,*args,a=1,b,**kwargs):
#             print(x,y)
#             print(args)
#             print(a)
#             print(b)
#             print(kwargs)
#
#         foo(1,2,3,4,5,b=3,c=4,d=5)
#         结果：
#             2
#             (3, 4, 5)
#             3
#             {'c': 4, 'd': 5}
#
# 此乃重点知识！！！