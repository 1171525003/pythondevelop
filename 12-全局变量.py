#coding=utf-8
#局部变量和全局变量 [当全局变量与局部变量同名时，在定义局部变量的子程序内,局部变量起作用，在其他地方全局变量起作用]
# name="wangchao" #全局变量
# def test():
#     global name #在函数内部定义一个全局变量
#     name="wangchao1"
#     print(name)
# test()
# #print(name1) 这样打印不出来是局部变量
# print(name)
# print(name)

#
# wang="lhf"
# def change_name():
#     wang="shuaideyibi"
#     wang1="shuaideyibi"
#     wang2="shuaideyibi"
#     wang3="shuaideyibi"
#     print('change_name',wang)
# change_name()
# print(wang)

#在子区间里面定义了变量的时候 调用该变量就是当前值，当在自权健没有定义的时候 全局变量有这个变量名的话 就使用这个全局变量
#想让子区间的局部变量变为，该脚本内部都能调用的时候使用 global这个函数


name="王大锤"
def weihou():
    name="小美"
    def weiweihou():
        global name
        name="铁山"
    weiweihou()
    print(name)
print(name)
weihou()
print(name)