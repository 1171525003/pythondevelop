#coding=utf-8
#递归特性，必须有一个明确的结束条件 return
#每一次进入更深层次的递归时候，问题规模都应该比上次递归要少
#递归效率不高层次过多会导致堆栈溢出
import time
# def calc(n):
#     print(n)
#     #time.sleep(4)
#     calc(n)
# calc(10)
#上面的会报错

def calc(n):
    print (n)
    if int(n/2) == 0:
        return n
    res=calc(int(n/2))
    return res
calc(10)