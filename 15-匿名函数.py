#coding=utf-8
# def calc(x):
#     return x+1
# res=calc(10)
# print(res)
# print(calc) #内存地址，加上（）代表执行
# print(lambda x:x+1)
# func=lambda x:x+1
# print(func(10))


name="alex"
def change_name(x):
    return name+"_sb"

res=change_name(name)
print(res)

res=lambda x:name+"_sb"
print(res(name))


func = lambda x,y,z:(x+1,y+1,z+1)
print(func(1,2,3,))