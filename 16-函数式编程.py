#coding=utf-8
#1、面向过程
#2、函数式
#3、面向对象


#例一：不可变，不用变量保存状态，不修改变量
#非函数式
# a=1
# def test():
#     global a
#     a+=1
#     return a
# test()
# print(a)
# #函数式
# n=1
# def test1(n):
#     return n+1
# print(test1(2))
# print(n)


#函数即变量
# def foo(n):
#     print(n)
# def bar(name):
#     print("my name is %s"%name)
# foo(bar)
# foo(bar()) #这样的事错误的 因为调用bar这个函数没有传值
# foo(bar("linei"))



#返回值之中包含函数
def bar():
    print("from bar")
def foo():
    print("from foo")
    return bar
a=foo()
a()

def hanle():
    print("from handle")
    return hanle
h=hanle()
h()

def test1():
    print("from test1")
def test2():
    print("from test2")
    return test1()
#test1()
test2()

#以上的是高阶函数
#1、函数接受的参数是一个函数名
#2、返回值中包含函数名