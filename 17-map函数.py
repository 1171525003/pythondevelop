#coding=utf-8
#map 处理一个序列中的，每个元素，得到的结果是一个”列表“，该列表元素个数与位置与原来一样
#filter 遍历序列中的每一个元素，判断每一个元素得到布尔值，得到的是True留下， print(list(filter(lambda p:p["age"]<=18),people)))
#reduce 处理一个序列然后吧序列进行合并操作  from functools import reduce;
num_1=[1,4,5,7]
def map_test(array):
    ret=[]
    for i in num_1:
        ret.append(i**2)
    return ret
ret=map_test(num_1)
print(ret)

#使用匿名函数
print("#"*16)
print(map(lambda x:x+1,num_1))
res=map(lambda x:x+1,num_1)
print(list(res))

#使用自定义函数
def double(x):
    return x**2
print(list(map(double,num_1)))


msg="wangshangshan"
print(list(map(lambda x:x.upper(),msg)))
msg.upper()