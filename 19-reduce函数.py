#coding=utf-8
from functools import reduce
num_1=[1,2,3,4]
# res=0
# for num in num_1:
#     res+=num
# print(res)

# def reduce_test(array):
#     res=0
#     for num in num_1:
#         res+=num
#     return res
# print(reduce_test(num_1))



#reduce函数 =>
# def multi(a,b):
#     return a*b
# # lambda x,y:x*y
# def reduce_test(func,array,init=None):
#     #res=array.pop(0)
#     if init is None:
#         res=array.pop(0)
#     else:
#         res=init
#     for num in num_1:
#         res=func(res,num)
#     return res
# print(reduce_test(multi,num_1,3))

res=reduce(lambda x,y:x*y,num_1,3)
print(res)

#map filter reduce 总结
#map 处理一个序列中的，每个元素，得到的结果是一个”列表“，该列表元素个数与位置与原来一样
#filter 遍历序列中的每一个元素，判断每一个元素得到布尔值，得到的是True留下， print(list(filter(lambda p:p["age"]<=18),people)))
#reduce 处理一个序列然后把序列进行合并操作  from functools import reduce;
#print(reduce(lambda x,y:x*y,range(1,101)))
#print(reduce(lambda x,y:x*y,range(1,101),2))