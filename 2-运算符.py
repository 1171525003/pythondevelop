#coding=utf-8
#成员运算符
name="吴国庆"
if "王" in name:
    print("yes")
else:
    print("no")

name="王超"
if "超" in name:
    print("yes")
else:
    print("no")

name="吴国庆"
if "吴庆" not in name:
    print("yes")
else:
    print("no")


v = 1 == 1
print(v)

v= not True
print(v)


#总结
#结果是值
#算术运算
a= 10 * 10
#赋值运算符
#a= a + 1 a+=1
#结果是布尔值
#比较运算
a = 1 > 5
#逻辑运算符
a= 1 > 6 or 1 == 1
#成员运算符
a = "吴" in "吴国庆"