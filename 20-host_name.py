#coding=utf-8
import socket
#获取本地的hostname和IP地址
# host_name=socket.gethostname()
# print("host_name",host_name)
# ip_address=socket.gethostbyname(host_name)
# print("ip_address:",ip_address)


#自定义函数获取远程IP地址
# def get_ipaddress_info():
# 	remote_host="v.qq.com"
# 	try:
# 		print("ip_address:%s"%socket.gethostbyname(remote_host))
# 	except socket.error as err_msg:
# 		print("%s:%s"%(remote_host, err_msg))
# if __name__="__main__":
# 	get_ipaddress_info()
# get_ipaddress_info()

#socket库，提供了很多不同IP地址库的格式的函数，这里我们使用：inet_aton();inet_ntoa(),转换IP地址 调用了binascii模块中的hexlify函数，以16进制的形式表示二进制数据
# from binascii import hexlify
# def convert_ipv4_address():
# 	for ip_addr in ["127.0.0.1","58.216.6.20"]:
# 		packed_ip_addr=socket.inet_aton(ip_addr)
# 		unpacked_ip_addr=socket.inet_ntoa(packed_ip_addr)
# 		print("IP address:%s => Packet:%s,Unpacked:%s"%(ip_addr,hexlify(packed_ip_addr),unpacked_ip_addr))
#
# convert_ipv4_address()


#通过指定的端口和协议找到服务名字 socket getservbyport()函数可以get到网络服务的使用端口
# def find_service_name():
# 	protocolname="tcp"
# 	for port in [80,25]:
# 		print("port:%s => service name :%s"%(port,socket.getservbyport(port,protocolname)))
# 	print("port:%s =>service name:%s"%(53,socket.getservbyport(53,"udp")))
# 	print("port:%s =>service name:%s"%(443,socket.getservbyport(443,"tcp")))
# find_service_name()


#主机字节序和网络字节序之间的相互之间的转换
#在编写底层网络应用的时候，有些时候需要处理通过电缆字啊两台设备上传送的底层数据，需要把主机操作系统发出的数据转换成网络格式，或者做逆向转换，因为这两种数据的转换方式不一样
# def convert_integer():
# 	data=1234
# 	#32bit
# 	print("Original:%s=>Longostbyte order:%s,Network byte order:%s"%(data,socket.ntohl(data),socket.htonl(data)))
# 	#16bit
# 	print("Original:%s => short hostbyte order:%s,Network byte order:%s"%(data,socket.ntohs(data),socket.htons(data)))
#
# convert_integer()
#将整数转换为网络字节序和主机字节序：notohl网络自己转换为长整型主机字节序，函数名字中的 n=>网络，h=>主机 l=>长整型 s=>短整型16位

#设定并获取默认的套接字超时时间
#你可以设置一个默认超时时间 gettimeout()，调用settimeout()方法，设定一个超时时间，这种操作在开发服务器应用的时候很有用
# def test_socket_timeout():
#     s= socket.socket(socket.AF_INET,socket.SOCK_STREAM) #创建套接字对象
#     print("Deafult socket timeout is :%s"%(s.gettimeout())) #获取套接字超时时间
#     s.settimeout(100) #设置套接字时间为100秒
#     print("Current socket timeout:%s"%s.gettimeout())
# test_socket_timeout()




#处理套接字错误
import sys
import socket
import argparse
def main():
    parser=argparse.ArgumentParser(description="socket error examples")
    parser.add_argument("--host",action="store",dest="host",required="False")
    parser.add_argument("--port",action="store",dest="port",type="int",required="False")
    parser.add_argument("--file",action="store",dest="file",required="False")
    given_args=parser.parse_args()
    host=given_args.host
    port=given_args.port
    filename=given_args.file
    #first try-except block --create socket
    try:


#这个不是公网解析，本地绑定host，之后在执行
# my_main="www.fansiren.net"
# print(socket.gethostbyname(my_main))
# print("port:%s => service name => %s "%(80,socket.getservbyport(80,"tcp")))
# print("port:%s => service name => %s "%(23,socket.getservbyport(23,"tcp")))
# print("port:%s => service name => %s "%(21,socket.getservbyport(21,"tcp")))
# print("port:%s => service name => %s "%(22,socket.getservbyport(22,"tcp")))
# print("port:%s => service name => %s "%(23,socket.getservbyport(23,"tcp")))
