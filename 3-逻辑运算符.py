#coding=utf-8
a=True
b=False
c=True
d=False
x= a and b
print("a and b",x)
x=b and a
print("b and a",x)

x= a and c
print("a and c",x)

x=b and d
print("b and d",x)

x=a or b
print("a or b",x)

x= a or c
print("a or c",x)


a= 1 > 6 or 1 == 1
print(a)

a=123
print(len(str(a)))
print(a.bit_length())

#str 内置函数
a="LAKDAISDadsda"
print(a.casefold()) #大小写转换
print(a.casefold().center(20,"-")) #总共20字符，并且以-填充空白
print(a.count('a')) #指定字符串出现几次
print(a.endswith("a")) #以指定字符结尾
print(a.startswith("A")) #以指定字符开始
print(a.find("AK")) #查找指定字符的位置索引

a="i AM is {name},age {age}"
print(a.format(name="wangchao",age=24)) #format格式化


a="nishis11_"
b="123123"
print(a.isalnum()) #字符串中是否只包含数字和字母
print(a.isdigit())
print(b.isdigit()) #如果字符串是数字字符串，则返回True，否则为false。

a="wangchao"
b="Wangchao"
c="123123"
print(a.islower()) #是否全是小写字符串
print(b.islower())
print(c.isnumeric()) #是否全是数字

a="1234567890\tqweqe"

print(a.expandtabs(5)) #已expandtabs里面的制定数字分割，没5个为一组，当到\T
#制表符的时候缺几个，就是打印出来的空格，不指定数字默认为8
a="username\temail\password\nwangchao\twangchao@qq.com\t123\nwangchao\twangchao@qq.com\t123\nwangchao\twangchao@qq.com\t123"
print(a.expandtabs(20)) #相当于一个表格了

a="你是谁？"
print(a)
t='=='
print(t.join(a))

list