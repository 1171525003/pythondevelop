#coding=utf-8
a=[1,2,3,4,5,6]
b=["one","two","three","one"]
a.append([7,8,9]) #append在行末添加,当成整体追加
print(a)
a.clear() #清空列表
print(a)
a=b.copy() #copy完整的列表
print(a)
print(a.count("one")) #指定字符串出现的次数

a.extend(["first","second"]) #在列表末尾追加列表，是列表不是字符，循环追加致后面
c=["a","b","b"]
a.extend(c) #或者以这样的形势追加
print(a)

print(a.index("b")) #查找索引位置,左边优先

a=[1,2,3,4,5,7]
a.insert(5,6) #在指定索引位置插入字符
print(a)
a.pop(5) #pop删除指定尾部字符串
print(a)
print(a.pop())#不加参数默认删除最后一个，并且获取删除的值
print(a)
# a.remove(3) #删除指定字符串
# print(a)
# a.reverse() #倒序排列
# print(a)
# a.sort()
# print(a) #正序排列
#
# del a[1:3] #按照切片的方式删除
# print(a)
#
# if 7 in a:
#     print("yes")
# else:
#     print("no")



v = "alex" #替换指定字符
print(v.replace("ex","6"))
v="1.2.3.4.5.6.7"
print(v.split(".",7)) #按照指定字符进行切片，后面number指的是，切片的次数