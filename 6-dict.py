#coding=utf-8
v={"a":1,"b":2}
print(v)
v.clear()
print(v)

v={"k1":18,
   "k2":True,
   "k3":(
       11,
       22,
       33,[(44,55)]
   )
   }
print(v.keys())
print(v.values())
print(v.items())
for k,l in v.items():
    print(k,l)
a=v.copy()
print(a)

p=dict.fromkeys(["k1",123,"999"],123) # 根据序列，创建字典，并指定统一的值
#{'k1': 123, 123: 123, '999': 123} 会打印出这个字典
'''
 @staticmethod # known case
    def fromkeys(*args, **kwargs): # real signature unknown
        """ Create a new dictionary with keys from iterable and values set to value. """
        pass
'''
print(p)

print(p.get("k2",123)) #get方法存在key返回值，不存在返回None，后面加参数返回该参数
print(p.get("k1"))
print(p.get("k2"))


print(p.pop("k1")) #删除指定key的值
print(p)
print(p.popitem()) #随机删除键值对
print(p)

print(v.setdefault("k1")) #查询dict中的键，存在返回vlaue，不存在返回none,后面加参数会生成新的字典
print(v.setdefault("k4",123))
print(v)

v.update({"k1":19,"K5":555}) #存在的更新，不存在的插入
print(v)
v.update(k1=20,k5=666) #支持这样的写法
print(v)



a={"a":"one","b":"two"}
for i,v in a.items():
    print(i,v)

for i,v in  enumerate(a): #去索引和
    print(i,v)
