# 整理
# 1、数字 int()
# 2、字符串 replace/find/join/strip/statswith/split/upper/lower/format
# 3、列表 append/extend/insert 索引，切片，循环
# 4、元组 无 索引，切片，循环 一级元素不能修改
# 5、字典 get/update/keys/values/items for,索引，切片，in
# 6、布尔值 0 1 bool() None "" () {} [] ==> Flase

#99乘法
# for i in range(1,10):
#     print("\n",end="")
#     for v in range(1,i+1):
#         print(str(v) + " * " + str(i) + "=",i*v,end="")


# #公鸡5元，母鸡3元，小鸡3只1元，100元买100只鸡，三种都要有，各要多少只
# for g in range(1,100//5):
#     for m in range(1,100//3):
#         for x in range(1,100):
#             if g + m + x == 100 and 5*g + 3*m + x/3 ==100:
#                 print(g,m,x)

#li=['alex','eric','rain'] 将列表里面的元素按照指定的连接符进行输出
# li=['alex','eric','rain']
# print("_".join(li))
# li=["one","two",123]
# li[2]=str(li[2])
# print(
#     "_".join(li)
# )

#tu=('alex','eric','rain')
# tu=('alex','eric','rain')
# print(len(tu)) #计算元组的长度并输出
# print(tu[1]) #获取元祖的第二个元素
# print(tu[1:])
# for i in tu: #for 输出元组的元素
#     print(i)
# for i  in range(len(tu)): #for range len 输出元组的索引
#     print(i)
# #使用enumrate
# print(list(enumerate(tu))) #enumrate 将一个可遍历的数据对象(如列表、元组或字符串)组合为一个索引序列
# for a,b in enumerate(tu,10): #索引从10开始增加
#     print(a,b)


# tu=("alex",[11,22,{"k1":"v1","k2":["age","name"],"k3":(11,22,33)},44])
# #一级元素不能被修改 alex不能被修改,k2对应的是list，可以修改,k3不能修改
# tu[1][2]["k2"].append("serven")
# tu[1][2]["k3"]
# print(tu)


# #nums=[2,7,11,15,1,8,7] 找出两个元素相加为9 的元素集合
# nums=[2,7,11,15,1,8,7]
# a=[]
# for i in nums:
#     for v in nums:
#         if i + v == 9:
#             a.append((i,v))
# print(a)
#
# #所有组合的索引
# a=[]
# for i in range(len(nums)):
#     for v in range(len(nums)):
#         if nums[i] + nums[v] ==9:
#             a.append((i,v,))
# print(a)




# tu=['alex','eric','rain']
# print(len(tu))
# tu.append("serven")
# print(tu)
# tu.insert(1,"kelly")
# print(tu)
# #del tu[2]
# print(tu.pop(2))

# for i  in  range(1,302):
#     print("alex"+ "-"+ str(i) + "\t" + "alex"+str(i)+"@live.com" +"\t"+"pwd"+str(i))
# while True:
#     user_list=[]
#     for i in range(1,302):
#         user_list.append(
#             {"name":"alex" +str(i),
#             "email":"alex@21.com"+str(i),
#              "passwd":"pwd"+str(i)
#              })
#     #print(user_list)
#
#     number=input("请输入查询的页码，每页10条数据:\n")
#     s=int(number)
#     start=(s-1)* 10
#     end=s*10
#     result=user_list[start:end]
#     for check in result:
#        print(check)