#coding=utf-8
print("i am is {name},and my age is {age}".format(name="wangchao",age="18"))

name="my name is %s,my baby is alex"%"wangchao"
print(name)

#多个参数以%()传入参数 %s可以传入一切
message="my name is %s ,and my friends name is %s"%("wangchao","beibei")
print(message)

#%d 只能传数字
age="my age is %d"%18
print(age)

#默认保存6位,默认四舍五入
msg="percent %f"%99.23456789
print(msg)
msg="percent %.3f"%99.23456789 #3位小数点
print(msg)

#截取字符
age="my age is %.4s"%"abcdef"
print(age)

#打印百分比 %f  两个%%表示打印百分比
age="my age is %.2f %%"%99.12345
print(age)

#拼接字符串
msg="i am %(name)s and age is %(age)d"%{"name":"alex","age":18}
print(msg)

msg="my name is %(pp).2f"%{"pp":123.3456}
print(msg)

#%号的使用
#%[(name)][flags][width].[precision]typecode
msg="my name is %(name)+30s myage is %(age)+30s"%{"name":"wangchao","age":"18"}
print(msg)

#颜色的使用
msg="my name is \033[43;1m%(name)+20s my age is \033[41;1m%(age)+10s"%{"name":"wangchao","age":"18"}
print(msg)