#coding=utf-8
msg="my name is {}age {} sex {}".format("wangchao","18","男")
print(msg)


#按照数字来
msg="my name is {0}age {2} sex {1}".format("wangchao","18","男")
print(msg)
#也可以按照这个来
msg="my name is {1} age is {1} sex {1}".format("wangchao","18","男")
print(msg)

#按照字典的形式
msg="my name is {name} age is {age} sex is {sex}".format(name="wangchao",age=18,sex="男")
print(msg)
