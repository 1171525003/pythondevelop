#coding=utf-8
# print(chr(97))
# print(ord('a'))
# print(pow(10,2)) #10的2次方
# print(pow(10,2,4)) #10的2次方除以2取余数
#
# l=[2,34,5,6,7,1,-1]
# print(list(reversed(l)))#反转
# b=sorted(l)
# print(list(reversed(b))) #先sort在reversed
#
# print(set("hello")) #创建集合
#
#
# L="0123456789"
# s1=slice(3,5)
# s2=slice(1,30,2) #步长
# print(L[s1])
# print(L[s2])
# print(s2.start,s2.stop,s2.step) #取出定义的值

# people=[
#     {"name":"alex","age":1000},
#     {"name":"alex1","age":999},
#     {"name":"alex2","age":103},
#     {"name":"alex3","age":18},
# ]
# print(sorted(people,key=lambda n:n["age"]))
# print(sorted(people,key=lambda n:n["name"]))
#
#
# people={"wangchao":98,"zhuchenyue":10,"linxiangyu":13}
# print(sorted(people,key=lambda value:people[value]))
#
#
# L={"one":12,"two":100,"three":34}
# print(list(L.keys()))
# print(sorted(L.keys()))
# print(sorted(L.values()))

L=[1,2,3,4,5]
print(sum(L))
print(sum(range(6)))


def test():
    msg="qwe啊实打实的"
    print(locals())
    print(vars())   #vars() 函数返回对象object的属性和属性值的字典对象。
test()

