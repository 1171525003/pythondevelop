#coding=utf-8
#打开文件，得到文件句柄，赋值给变量
# f=open('王北车',encoding="utf-8")
# data=f.read()
# print(data)
# f.close()

#文件打开模式 r w  a
# f=open('王北车','r',encoding="utf-8")
# # data=f.read()
# # print(data)
# print(f.readable())
# print(f.readline())
# print(f.readlines()) #打印成列表
# f.close()


# f=open('王北车1','w',encoding='utf-8')
# print(f.writable())
# f.write("xiaoxiao\nbeibei\nnishinishi\n")
# f.writelines(['1111\n','666666\n','wangchaowangchao\n'])
# f.close()


#只读模式
# f=open('王北车2','a',encoding='utf-8')
# f.write('11111\n')
# f.close()

#r+读写 w+可读写   从前面写
#
# f=open('王北车2',"r+",encoding='utf-8')
# f.write('nima')


# with open("王北车2",'a') as f:
#     f.write('wangbeihe\n')
#


#源文件写到新文件
# with open("王北车",'r',encoding='utf-8') as wang1,\
#     open("王北车2",'w',encoding='utf-8') as wang2:
#     data=wang1.read()
#     wang2.write(data)




#r 只读模式【默认模式，文件必须存在，不存在报异常】
#w 只写模式【不可读，不存在则创建，存在则清空内容】
#x 只写模式【不可读，不存在则创建，存在则报错】
#a 追加模式【可读，不存在则创建，存在则只追加内容】

#"+" 表示可以同时读写某个文件
#r+ 读写【可读，可写】
#w+ 写读【可读，可写】
#x+ 写读【可读，可写】
#a+ 写读【可读，可写】

#"b" 表示以字节的方式操作
#rb 或者 r+b
#wb 或者 w+b
#xb 或者 x+b
#ab 或者 a+b

#
# with open('王北车2','rb') as data:
#     print(data.read().decode('utf-8'))
#
# a="王朝"
# print(a.encode('utf-8'))
# print('王朝'.encode('utf-8'))
# print('王朝'.encode('gbk'))


# f=open('王北车2','r',encoding='utf-8')
# print(f.read())
# print(f.encoding)  #文件打开的编码
# print(f.closed)    #检测文件是否关闭，关闭返回True，否则返回False
# # f.close()
# print(f.closed)
# f.flush()          #flush() 方法是用来刷新缓冲区的，即将缓冲区中的数据立刻写入文件，同时清空缓冲区，不需要是被动的等待输出缓冲区写入。

f=open('王北车1','rb')
#print(f.read())
print("_______"*18)
f1=f.readline()       #使用readline的时候前面不能使用read() 这样文件就会被读完；没有输出
print(f1.decode('utf-8'),end='')
#print(f.readlines())
print(f.readlines() #在文件读取操作的时候都有位置，当之前的过程中读完文件，下面在读取的时候就会为空
f.seek(0) #控制光标的移动
print(f.readline(),end='')
print(f.tell())          #tell() 方法返回文件的当前位置，即文件指针当前位置
f.seek(4)          #光标向后移动一个在打印出来，注意这个地方，中文字符是三个字节
print(f.readline())
# print(f.read(1))
#print(f.truncate(1))     #截断文本



# 总结：read(3) 代表读取3个字符
# 其余的文件内光标移动都是以字节为单位，seek，tell，read，truncate
# flush 将文件内容从内存刷到硬盘
# closed 要是一件关闭返回True
# encoding 查看使用的打开文件的编码
# tell 查看当前光标的位置
# seek(3) 从头开始光标移动三个字节
# truncate(10) 文件必须以写方式打开，w和w+除外
