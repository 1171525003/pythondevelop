#conding=utf-8
#迭代器协议：对象必须提供一个next方法，执行该方法，要么返回迭代中的下一项，
#要么就引起stoplteration异常，以终止迭代；（只能往后走不能往前进）
#可迭代对象（对象内部定义了一个__iter__()方法）
#协议是一种约定，可迭代对象实现了迭代器协议，python的内部工具，（for,sum,max,min） 等使用迭代器协议访问对象


L=[1,2,3]
# for i in L:
#     print(i)
ok=iter(L)
print(next(ok))
print(next(ok))
print(next(ok))
print(next(iter(L)))
print(next(iter(L)))
print(next(iter(L)))
print(next(iter(L)))
print(next(iter(L)))

name="wangchao"
iter_test=name.__iter__()
print(iter_test)
print(list(iter_test))
# print(iter_test.__next__())
# print(iter_test.__next__())
# print(iter_test.__next__())
# print(iter_test.__next__())
# print(iter_test.__next__())
# print(iter_test.__next__())
# print(iter_test.__next__())
# print(iter_test.__next__())
#next内置函数调用的就是iter.__next__()这个迭代器函数
#判定一个对象是不是迭代对象 可以看是否支持iter
index=0
while index<len(L):
    print(L[index])
    index+=1

