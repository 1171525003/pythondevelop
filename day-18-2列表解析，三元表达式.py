#coding=utf-8
#三元表达式
# name="alex"
# res="SB" if name == "alex" else "帅哥"
# print(res)
#
#
# #列表解析
# egg_list=[]
# for i in range(10):
#     egg_list.append("鸡蛋%s"%i)
# print(egg_list)
#
# L=["鸡蛋%s"%i for i in  range(10)]
# L1=["鸡蛋%s"%i for i in  range(10000) if i >5]
# print(L)
# print(L1)
#
# def test():
#     list1=[]
#     for i in range(1000):
#         list1.append(i)
#     return list1
# print(test())
#
# def test2():
#     list2=[]
#     for i in range(1000):
#         list2.append(i)
#         yield list2
# print(test2())
# print(test2())
# print(test2())


# 容器是一系列元素的集合，str、list、set、dict、file、sockets对象都可以看作是容器，容器都可以被迭代（用在for，while等语句中），因此他们被称为可迭代对象。
# 可迭代对象实现了__iter__方法，该方法返回一个迭代器对象。
# 迭代器持有一个内部状态的字段，用于记录下次迭代返回值，它实现了__next__和__iter__方法，迭代器不会一次性把所有元素加载到内存，而是需要的时候才生成返回结果。
# 生成器是一种特殊的迭代器，它的返回值不是通过return而是用yield。


# L="chao.wang"
# #print(dir(L))
# L1=L.__iter__()
# print(L1)
# print(list(L1))
# print(L1.__next__())
# print(L1.__next__())
# print(L1.__next__())
# print(L1.__next__())
# print(L1.__next__())
# print(L1.__next__())
# print(L1.__next__())
# print(L1.__next__())

# print("--"*10)
# for i in L:  #i_1=L.__iter__() i_1.next  先把这个列表变成可迭代对象，遵循可迭代协议，捕捉到结束异常StopIteration结束
#     print(i)

#__iter__() 遵循迭代器协议，生成可迭代对象


print("生成器方法一：")
laomuji=("老母鸡%s" %i for i in range(10) if i >5)
print(laomuji)
print(laomuji.__next__())
print(laomuji.__next__())
print(laomuji.__next__())
print(next(laomuji))

print("生成器方法二：")
def test1():
    yield "老母鸡1"
    yield "老母鸡2"
    yield "老母鸡3"
    yield "老母鸡4"
    yield "老母鸡5"
g=test1() #在这个地方一定要赋值不然你下面直接prin test1（）的时候 会反复取出yield1 的值
print(g.__next__())
print(g.__next__())
print(g.__next__())
print(g.__next__())
print(g.__next__())


#[i for i in range(10)] 列表解析
#(i for i in range(10)) 生成器表达式

# def test(): 生成器函数
#     yield 1
#g=test()
#print(g.__next__())
#print(next(g))