#coding=utf-8
def test():
    yield 1
    yield 6
g=test()
print(g)
print(g.__next__())
print(g.__next__())

#三元表达式
#name="alex"
name="asd"
g="SB" if name == "alex" else "帅哥!"
print(g)

#列表解析
egg=[]
for i in range(10):
    egg.append('鸡蛋%s'%i)
print(egg)
print("列表解析-表达式")
l=['鸡蛋%s'%i for i in range(10) if i > 5]
print(l)

def test1():
    for i in range(5):
        yield "老母鸡%s"%i
g_i=test1()
print(g_i.__next__())
print(g_i.__next__())
print(g_i.__next__())
print(g_i.__next__())
print(g_i.__next__())
print(g_i.__next__())