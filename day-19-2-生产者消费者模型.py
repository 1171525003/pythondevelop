#coding=utf-8
import time
# def producer():
#     ret=[]
#     for i in range(10):
#         time.sleep(0.1)
#         ret.append("包子%s" %i)
#     return ret
#
# def consumer(res):
#     for index,baozi  in enumerate(res):
#         time.sleep(0.1)
#         print("第%s个人，吃了%s"%(index,baozi))
#
# res=producer()
# consumer(res)

#要求：利用生成器将生产与消费同步进行，不是迭代生产完在让消费者进行消费【两个程序同时生产，第一个函数的生产值得出，之后交由第二个函数在执行】
#触发迭代器运行的操作send()函数，必须加参数

# def test1():
#     print("开始啦！")
#     first=yield 1
#     print("第一次！",first)
#     yield 2
#     print("第二次！")
# t=test1()  #只是拿到了生成器
# #g1=next(t) #返回第一个print
# #print(g1) #返回yield值
# #print(t.__next__()) #直接调作用next会直接执行到yield这一步（并且返回结果）
# #触发生成器的运行  t.send(None)必须传参数
# #t.send(None) 1、可以触发生成器向下执行，2、将值传给yield 在使用send之前必须是使用过next之后，不然会报错（不能将非NONE值发送给刚刚启动的生成器，当你传一个空值的时候，又会报错：必须要带一个参数）
# #print(t.__next__())
# s=t.send()
# print(s)

#send方法和next方法唯一的区别是在执行send方法会首先把上一次挂起的yield语句的返回值通过参数设定，从而实现与生成器方法的交互。但是需要注意，
#在一个生成器对象没有执行next方法之前，由于没有yield语句被挂起，所以执行send方法会报错。
#send方法有一个参数，该参数指定的是上一次被挂起的yield语句的返回值。
def consumer(name):
    print("我是[%s],我准备开始吃包子了"%name)
    while True:
        baozi=yield
        time.sleep(0.1)
        print("%s 很开心的把[%s]吃掉了"%(name,baozi))
def producer():
    c1=consumer('吴佩奇')
    c2=consumer('王上山')
    c1.__next__()
    c2.__next__()
    for i in range(10):
        time.sleep(0.1)
        c1.send("包子%s"%i)
        c2.send("包子%s"%i)
producer()

