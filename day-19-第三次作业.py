#coding=utf-8
#bool类型中的False值的几种情况
# print(bool(''))
# print(bool(0))
# print(bool(()))
# print(bool([]))
# print(bool({}))
# print(bool(False))

#根据范围获取3和7整除的所有数字的和，并且返回调用者
#符合条件的数字个数以及符合条件的数字的总和
# def func(start,end):
#     ret=[]
#     for i in range(start,end):
#         if i%7 == 0 or i%3 ==0:
#            ret.append(i)
#     return ret
# a=func(1,100)
# su=sum(a)
# print(su)


#三元表达式
# ret=1 if 1>1 else 5
# print(ret)

#set获取两个列表中相同的元素集合
# L1=[11,22,33]
# L2=[22,33,44]
# L3=set(L1)
# L4=set(L2)
# print(L3 & L4)
# print(L3 | L4)

#定义一个函数统计一个字符串中的大写字母，小写字母，数字的个数，并以字典为结果返回给调用者
# def check_any(n):
#     islower=[]
#     isupper=[]
#     number=[]
#     for i in n:
#         if i.islower() == True:
#             islower.append(i)
#         elif i.isupper() == True:
#             isupper.append(i)
#         elif i.isdigit() == True:
#             number.append(i)
#         else:
#             pass
#     return ("该字符串中小写是：",islower,"小写的字符数量是：",len(islower),"该字符串中大写字母是：",isupper,"大写数量是：",len(isupper),"该字符串中数字是：",number,"数字的个数是：",len(number))
# a=check_any("shshsh2BNM")
# print(a)


#简述pytohn3中的range函数和python2.7中的函数有什么区别
#3.X range不会生成值，只有在用的时候，才会生成
#2.7 range会直接生成一个列表，值已经生成

# #书写运行结果
# a="oldboy %"
# print(a)
#
# b="oldboy %d %%"%(12,)
# print(b)

#简述对象和类的关系
#如果值是某类型，那这个值就是这个类的对象

#手写答案 [此处 两个重名的函数，以后者为准，后者向前者覆盖]
# def func(a1):
#     return a1+100
# func =lambda a1:a1+200
# ret = func(10)
# print(ret)

#内置函数all和any的区别 all[空，返回True；非空全为真为真，否则返回假] any[只有一个为真就为真]
# L=[1,2,3]
# L1=[]
# L2=[0,1,2,3]
# L3=[0]
# L4=[0,1,2,3]
# print(all(L))
# print(all(L1))
# print(all(L2))
# print(any(L3))
# print(any(L4))

#字符串转换成utf-8
# l="老男孩"
# print(l.encode('utf-8'))
# a=bytes(l,"utf-8")
# print(a)

#使用zip内置函数实现s="alex_is_good_guy"
# l1=["alex",22,33,44,55]
# l2=["is",22,33,44,55]
# l3=["good",22,33,44,55]
# l4=["guy",22,33,44,55]
# print("_".join(list(zip(l1,l2,l3,l4))[0]))
# print(list(zip(l1,l2,l3,l4))[1])


# name='root'
# def func():
#     name="seven"
#     def outer():
#         name="eric"
#         def inner():
#             global name
#             name="蒙蔽了吧..."
#             print("okok1")
#         print("okok2")
#     print("2",name)
#     outer()
#
# ret=func()
# print("3",ret)
# print("4",name)

# name='root'
# def func():
#     name='seven'
#     def outer():
#         name='eric'
#         def inner():
#             global name
#             name ='闷逼了吧。。。'
#         print("第一步:",name)
#     o=outer()
#     print("第二步",o)
#     print("第三步",name)
# ret=func()
# print("第四步",ret)
# print("第五步",name)

#有几点注意的：
# func 不调用的话是不会被执行的
#函数没有return 当a=func() 这个会返回None的 如果直接调用的话是会执行函数内部的代码的比如print

# name="苍老师"
# def outer(func):
#     name='alex'
#     func()    #结果就是执行show()
# def show():
#     print(name)
# outer(show)
#上面的别想得太复杂，首先outer(show) 就是将show这个函数（也就相当于一个变量，在内存中占一个内存地址,print(name) 这个name是全局变量 是苍老师）
#这个时候其实 func和show同样指向一个内存地址，func() 就是show() 就相当于直接show()


# name="苍老师"
# def outer():
#     name="波多"
#     def inner():
#         print(name)
#     return inner()
# ret=outer()
# print(ret)

# name="苍老师"
# def outer():
#     name="波多"
#     def inner():
#         print(name)
#     return inner
# print("开始了")
# ret=outer()  #没有返回值根本就没执行，直接跳过
# print("下面是第二步")
# ret()    #就相当于执行inner()
# print("这是第三步",ret)
# print("下面是第四部")
# result=ret()
# print("最后一步",result)



# name="苍老师"
# def outer():
#     name="波多"
#     def inner():
#         print(name)
#     return inner
# a=outer()
# print(a)
# a()
# print(outer())
# #outer()

l=[i for i in range(10)]
print(l)