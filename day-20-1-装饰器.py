#coding=utf-8
#装饰器：本质就是，为其他函数添加附加功能
#1、不修改被修饰函数的源代码
#2、不修改被修饰函数的调用方式

#装饰器=高阶函数+函数嵌套+闭包

import time
def cal(l):
    res=0
    for i in l:
        time.sleep(0.1)
        res+=i
    return res
l=cal(range(5))
print(l)

#为上面的函数添加功能，不修改源代码，不修改调用的方式


#上面的
#1、没有嵌套
#2、有闭包
#3、不是高阶函数，参数不是函数名，返回值也不是函数名