#coding=utf-8
# user_dic={'username':None,'login':False}  #定义一个状态
# def auth_func(func):
#     def wrapper(*args,**kwargs):
#         if user_dic['username'] and user_dic['login']: #满足状态的时候直接执行代码
#             res = func(*args, **kwargs)
#             return res
#         username=input('京东用户名：').strip()
#         passwd=input('京东密码：').strip()
#         if username == 'wangchao' and passwd == 'qwe':
#             user_dic['username']=username  #将用户名密码赋值给状态，闭包的概念，向上层函数查找
#             user_dic['login']=True
#             res=func(*args,**kwargs)
#             return res
#         else:
#             print('您的账户名密码输入错误')
#     return wrapper
# @auth_func
# def index():
#     print('欢迎来到京东主页')
#
# @auth_func
# def home(name):
#     print('欢迎回家%s'%name)
#
# @auth_func
# def shopping_car():
#     print('购物车里有{}，{}，{}'.format('naicha','hanbaobao','niunai'))
#     print('购物车里有{name1},{name2},{name3}'.format(name1='奶茶',name2='汉堡包',name3='牛奶'))
# index()
# home('滁州')
# shopping_car()
#
user_default={'name':None,'login':False}
def zhuangshi(func):
    def zizhuangshizi(*args,**kwargs):
        if user_default['name'] and user_default['login']:
            res=func(*args,**kwargs)
            return res
        username=input('JD用户名：').strip()
        passwd=input('JD密码：').strip()
        if username == 'wangchao' and passwd == '123':
            user_default['name']=username
            user_default['login']=True
            res=func(*args,**kwargs)
            return res
        else:
            print('JD账户名密码输入错误')
    return zizhuangshizi
@zhuangshi
def home(name):
    print('欢迎回家%s'%name)
@zhuangshi
def shopping_car():
    print('购物车里有：{name1},{name2},{name3}'.format(name1='奶茶',name2='香蕉',name3='袜子'))

print('before--->',user_default)
home('王上山')
print('aftre--->',user_default)
shopping_car()