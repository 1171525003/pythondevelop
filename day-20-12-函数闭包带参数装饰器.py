#coding=utf-8
userlist=[                                  #用户信息列表
    {'name':'wangchao','passwd':'qwe'},
    {'name':'王上山','passwd':'ewq'},
    {'name':'朱成成','passwd':'qwe'},
    {'name':'林祥祥','passwd':'qwe'}
]
userinfo={'name':None,'login':False} #用户登录状态
def auth(auth_type='filedb'):
    def zhuangshiqi(func):
        def zizhaungshiqi(*args,**kwargs):
            print('认证类型是：',auth_type)
            if auth_type == 'filedb':
                if userinfo['name'] and userinfo['login']:
                    res=func(*args,**kwargs)
                    return res
                username = input('JD用户名：').strip()
                passwd = input('JD密码：').strip()
                for uslist in userlist:
                    if username == uslist['name'] and passwd == uslist['passwd']:
                        print('登录成功！')
                        userinfo['name'] = username
                        userinfo['login'] = True
                        res=func(*args,**kwargs)
                        return res
                else:
                    print('用户名密码错误！')
            else:
                print('什么吊东西，没%s这个认证类型'%auth_type)
        return zizhaungshiqi
    return zhuangshiqi
@auth(auth_type='filedb')
def home(name):
    print('欢迎回家%s'%name)
@auth(auth_type='ssl')
def shopping_car():
    print('购物车里有：{name1},{name2},{name3}'.format(name1='奶茶',name2='香蕉',name3='袜子'))

print('before--->',userinfo)
home('王上山')
print('after--->',userinfo)

# print('before--->',userinfo)
shopping_car()
# print('after--->',userinfo)
