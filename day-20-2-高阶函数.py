#coding=utf-8
#高阶函数的定义：
#1、函数接受的参数是一个函数名
    #作用：在不修改函数源代码的前提下，为函数添加新功能
    #不足：会改变函数的调用方式
#2、函数的返回值是一个函数名
    #作用: 不修改函数的调用方式
    #不足: 不能添加新功能
#3、满足上述条件的任意一个，都可称为高阶函数
import time
#情况1：函数接受的参数是一个函数名
# def test1():
#     time.sleep(3)
#     print("你好")
#
# def test2(func):
#     print(func)
#     start_time=time.time()
#     func()
#     stop_time=time.time()
#     print("耗时%s"%(stop_time-start_time))
# test2(test1)

# def  test1():
#     print("你好，函数一")
# def test2(func):
#     return func
# res=test2(test1)
# print(res)
# res()               #只要有函数的内存地址就好办加括号就能运行[只有见到这个括号()，程序会根据函数名从内存中找到函数体，然后执行它]

#情况二：不改变函数的调用方式
# test1=test2(test1)
# test1()


def foo():
    time.sleep(3)
    print("来自foo")
#不修改foo源代码
#不修改foo调用方式

#多运行了一步
# def timer(func):
#     start_time=time.time()
#     func()
#     stop_time=time.time()
#     print("耗时：%s"%(stop_time-start_time))
#     return func #在这个地方有执行了一步所以多了一个
# foo=timer(foo)
# foo()
