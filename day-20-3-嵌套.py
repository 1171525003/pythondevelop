#coding=utf-8
#在函数内部，重新定义了一个函数
# def test1():
#     print("test1")
#
# def test2():
#     print("test2")
#     test1()
# test2()
#上面的不叫函数嵌套，别弄混了

def father(name):
    print('from father %s'%name)
    def son():
        print('from son')
        def grandson():
            print("from grandson")
        print(locals())
        grandson()
    son()
    print(locals())
father("wang")
