#coding=utf-8
#python中的闭包从表现形式上定义（解释）为：如果在一个内部函数里，对在外部作用域（但不是在全局作用域）的变量进行引用，那么内部函数就被认为是闭包(closure)
#当内嵌函数体内，引用到体外变量的时候，将会把定义时涉及到的引用环境和函数体，打包成一个整体（闭包）返回。
#引用环境： 是指在程序执行过程中的某个点所有处于活跃状态的约束（一个变量的名字和其所代表的对象之间的联系）所组成的集合。
#由于闭包把函数和运行时的引用环境打包成一个新的整体，就解决了函数编程中的嵌套所引发的问题

def father(name):
    print('from father name %s'%name)
    def son():
        print('from son name %s'%name)
        def grandson():
            name='wangchao1'
            print("from grandson %s"%name)
        grandson()
    son()
father("wangchao3")

