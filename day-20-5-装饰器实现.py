#coding=utf-8
#装饰器的框架
import time
def timmer(func):  #func=test
    def wrapper():
        start_time=time.time()
        print(func) #wrapper函数打印出内存地址
        func()  #这里并没有func向上层查找（闭包）找到了timmer(func) ,这里的func=test
        stop_time=time.time()
        print('耗时：%s'%(stop_time-start_time))
    return wrapper
@timmer
def test():
    time.sleep(3)
    print('test函数运行完毕')

#方法一：错误的调用方式
# res=timmer(test) #返回的是wrapper地址
# res()  #执行wrapper函数

#方法二：正确的调用方式
# test=timmer(test) #为了不更改test函数的调用方式
# test() #原来的调用方式
#上述方法可用但是当函数多的时候 这样的赋值操作是不明智的

#方法三：语法糖
#@timmer @装饰器  就相当于test=timmer(test)
#注意这里使用语法糖的位置是在timmer(func) 装饰器与test()函数之间
#之后可以直接test()调用打印结果

test()