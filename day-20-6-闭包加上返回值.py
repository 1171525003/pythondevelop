#coding=utf-8
import time
def timmer(func):
    def wrapper():
        start_time=time.time()
        print(func)
        func()  #这个时候返回的是wrapper的return 此时想返回下面的test return res=func() 这样就能解决问题
        stop_time=time.time()
        print('耗时：%s'%(stop_time-start_time))
        return '123' #返回这个return  #return res
    return wrapper

@timmer

def test():
    time.sleep(3)
    print('test函数运行结束！')
    return '这是test函数的返回值'
res=test()
print(res)