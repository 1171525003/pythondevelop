#coding=utf-8
import time
def timmer(func):
    def wrapper(*args,**kwargs):
        start_time=time.time()
        print(func)
        res=func(*args,**kwargs)
        stop_time=time.time()
        print('耗时：%s'%(stop_time-start_time))
        return res
    return wrapper

@timmer

def test(name,age):
    time.sleep(3)
    print('test函数运行结束！%s->%s'%(name,age))
    return '这是test函数的返回值'
# res=test('wangchao',18)
# print(res)


#下面举例说明
def test2(name,age,gender): #test2(*('alex','18','male').**{})
    #name,age,gender=('alex','18','male')  解压序列，类似于位置参数一一对应，不能多不能少
    print(name)
    print(age)
    print(gender)

def test1(*args,**kwargs):
    test2(*args,**kwargs)   #args=('alex','18','male') kwargs={}
test1('alex','18','male')