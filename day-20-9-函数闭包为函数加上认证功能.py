#coding=utf-8
user_dic={'username':None,'login':False}
def auth_func(func):
    def wrapper(*args,**kwargs):
        username=input('京东用户名：').strip()
        passwd=input('京东密码：').strip()
        if username == 'wangchao' and passwd == 'qwe':
            res=func(*args,**kwargs)
            return res
        else:
            print('您的账户名密码输入错误')
    return wrapper
@auth_func
def index():
    print('欢迎来到京东主页')

@auth_func
def home(name):
    print('欢迎回家%s'%name)

@auth_func
def shopping_car():
    print('购物车里有{}，{}，{}'.format('naicha','hanbaobao','niunai'))
    print('购物车里有{name1},{name2},{name3}'.format(name1='奶茶',name2='汉堡包',name3='牛奶'))
index()
home('滁州')
shopping_car()

