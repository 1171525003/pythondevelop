#coding=utf-8
def fetch(data): #查询
    # print('用户数据是：',data)
    backend_data='backend %s'%data
    with open('linnx.txt','r',encoding='utf-8') as read_f:
        tag=False
        B=0
        for read_line in read_f:
            B+=1
            # print('这是第%s次循环'%B,read_line.strip())
            # print('这是第%s次循环'%B,backend_data)
            if read_line.strip() == backend_data:
                tag=True
                continue
            if tag and read_line.startswith('backend'): #出现下一次backend之前结束
                    break
            if tag:
                print('第%s次目标数据是->'%B,read_line,end='')
            if read_line != backend_data and not tag:
                print('全文搜索完毕，未匹配到数据')
                break

#fetch('www.baidu.com')

def add(data):  #增加
    print('add->%s'%data)

def modify(): #修改
    pass
def delete():
    pass
def quit():
    pass

if __name__=='__main__':
    msg='''使用方法：
    1：查询
    2：添加
    3：修改
    4：删除
    5：退出
    '''
    msg_dic={
        '1':fetch,
        '2':add,
        '3':modify,
        '4':delete,
        '5':quit,
    }
    while True:
        print(msg)
        choice=input('please input your choice:').strip()
        if not choice:continue
        if choice == '5':break

        data=input('please input data for fetch:').strip()
        msg_dic[choice](data)
        break