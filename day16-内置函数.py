# coding=utf-8
print(abs(-1))  # 绝对值
print(all([1, 2, 3, 4, "one", 1, "2", ""]))  # 做布尔运算，其中的元素为空或者0 返回False
print(all(["1"]))

print(any(["", "1"]))  # 与all相反，有一个为真就都为真
print(any([""]))

print(bin(3))  # 转换成二进制
print(hex(3))  #16进制
print(oct(3))  #8进制
print(bool(0))  # 空，None，0的布尔值为False 其他的都是 True

print(bytes("你好", encoding='utf-8'))  # 手动将字符串做编码
print(bytes("你好", encoding="utf-8").decode("utf-8"))  # 解码
print(bytes("你好", encoding="gbk"))  # gbk编码
print(bytes("你好", encoding="gbk").decode("gbk"))  # 解码
# print(bytes("你好",encoding="ascii")) #ascii 不能对中文进行编码

print(chr(97))  # 97这个十进制数字对应的ascii表中的是a

print(dir(all))  # 打印某一个对象下面都有哪些方法
print(dir(dict))

print(divmod(10, 3))  # 取商得余数，写博客的时候可以做分页功能

print(list(enumerate('one')))  # 索引迭代器
print(list(enumerate(["one", "two"])))

dic = {"name": "wangchao"}
str_dic = str(dic)
print(str_dic)  # 将字典传给字符串
print(eval(str_dic))  # 提取字符串中的原始值
express = "12*3"
print(eval(express))

a=frozenset(["one","two","three"]) #构建一个不可变的，无序的唯一元素集合。
print(a)

#可以hash的数据类型即不可变数据类型，不可hash的数据类型即可变数据类型
print(hash('123123123')) #hash值不能被反推，每次运行都会变化
#可以做文件真实性校验，比如文件下载，网站上hash，用户下载完成hash，（同时发生的），对比一下，可进行验证

print(isinstance("wangchao",str)) #返回True or False
print(isinstance(123,int))

name="哈哈哈"
print(globals())  #全局变量
print(locals())  #局部变量 （这里主要是在def 函数时候的区别）
print(__file__) #相对路径
