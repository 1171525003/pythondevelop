#coding=utf-8
# f=open('wangchao.txt','rb')  #文本中包含中文的时候们需要对open进行rb
# print(f.read().decode('utf-8'))
# f.seek(0)
# print("-------------")
# print(f.tell())
# f.seek(10,0)   #0代表从文件开头，跳10个字节
# print(f.tell())
# f.seek(10,1)  #1代表是从文件上次句柄所在的位置开始计算10个[相对位置]，使用这个功能的时候必须用rb打开文件，因为seek是按照字节来的
# print(f.tell())
# f.seek(3,1)
# print(f.tell())
# print(f.readline())
# print(f.readline())
# print(f.readline())
# print(f.readline())
# print(f.readline().decode('utf-8'))
# print(f.readline(3).decode('utf-8'))
# f.seek(-20,2)    #2代表倒序
# print(f.tell())
# print(f.readline(10))

# fl=open("wangchao.txt",'r')
# print(fl.read())



# 总结：read(3) 代表读取3个字符
# 其余的文件内光标移动都是以字节为单位，seek，tell，read，truncate
# flush 将文件内容从内存刷到硬盘
# closed 要是一件关闭返回True
# encoding 查看使用的打开文件的编码
# tell 查看当前光标的位置
# seek(3) 从头开始光标移动三个字节
# truncate(10) 文件必须以写方式打开，w和w+除外


# f=open("wangchao.txt",'rb')
# for i in f:
#     print(i.decode('utf-8'),end='')


f=open('wangchao.txt','rb')
for i in f:
    offs=-10
    while True:
        f.seek(offs,2)
        data=f.readlines()
        if len(data)>1:
            print("文件的最后一行是\n%s"%data[-1].decode('utf-8'))
            break
        offs*=2