#coding=utf-8
count=1 #while输出1-10
while count<11:
    print(count)
    count+=1

for i in range(101): #奇数偶数
    if i % 2 == 0:
        print(i,"是偶数")
    else:
        print(i,"是奇数")

n=1
a=0
while n < 101:
    a=a+n
    n=n+1
print(a)


x=1
y=0
while x<100:
    if x % 2 ==0:
        y=y-x
    else:
        y=y+x
    x=x+1
print(y)